// Copyright Will Nations 2015

#pragma once

#include "Object.h"
#include "Engine.h"
#include "TargetChannel.h"
#include "Targeter.h"
#include "Targetable.h"
#include "TargetingSystem.generated.h"

USTRUCT(BlueprintType)
struct MECHINATIONS_API FTargetKey {
    GENERATED_USTRUCT_BODY()

    FTargetKey(TScriptInterface<ITargeter> Targeter = TScriptInterface<ITargeter>(),
               FTargetChannel Channel = FTargetChannel()) 
        : Targeter(Targeter), Channel(Channel) {}

    TScriptInterface<ITargeter> Targeter;
    FTargetChannel Channel;
};

/**
 * 
 */
UCLASS()
class MECHINATIONS_API UTargetingSystem : public UObject
{
    GENERATED_BODY()

private:

    static TUniquePtr<UTargetingSystem> instance;

    static TMap<FTargetKey, TScriptInterface<ITargetable>> Targets;

public:
    UTargetingSystem();

    UFUNCTION(BlueprintCallable, Category="Targeting")
    static UTargetingSystem* Instance() {
        if (!UTargetingSystem::instance.IsValid()) {
            UTargetingSystem::instance.Reset(new UTargetingSystem());
        }
        return UTargetingSystem::instance.Get();
    }
    
};
