// Copyright Will Nations 2015

#pragma once

#include "Object.h"
#include "TargetChannel.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct MECHINATIONS_API FTargetChannel { 
    GENERATED_USTRUCT_BODY()

    FTargetChannel(const FString ChannelName = FString("")) : ChannelName(ChannelName) {}

    UPROPERTY()
    FString ChannelName;
    
};
