// Copyright Will Nations 2015

#pragma once

#include "Object.h"
//#include "TargetChannel.h"
#include "Targetable.h"
#include "Targeter.generated.h"

class UTargetingSystem;

/**
* An interface for any object that acquires a target associated with the TargetingSystem instance
*/
UINTERFACE(Blueprintable)
class MECHINATIONS_API UTargeter : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class MECHINATIONS_API ITargeter {
    GENERATED_IINTERFACE_BODY()

public:
    /**
    * Identifies the Targeter's current target for a given channel.
    * @param Channel - The Channel with which to check for a target
    * @return A reference to the Targeter's current target on the given channel
    * 
    */
    UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Targeting")
    TScriptInterface<ITargetable> GetTarget();

};


