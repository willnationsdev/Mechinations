// Copyright Will Nations 2015

#pragma once

#include "Object.h"
#include "Targetable.generated.h"

/**
 * 
 */
UINTERFACE(Blueprintable)
class MECHINATIONS_API UTargetable : public UInterface
{
    GENERATED_UINTERFACE_BODY()
};

class MECHINATIONS_API ITargetable {
    GENERATED_IINTERFACE_BODY()

public:

    /*
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Targeting")
    FVector GetLocation() const;

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Targeting")
    FString GetName() const;

    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Targeting")
    FString GetDescription() const;
    */

};


