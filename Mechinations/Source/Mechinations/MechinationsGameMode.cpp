// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "Mechinations.h"
#include "MechinationsGameMode.h"
#include "MechinationsPlayerController.h"
#include "MechinationsCharacter.h"

AMechinationsGameMode::AMechinationsGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AMechinationsPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}